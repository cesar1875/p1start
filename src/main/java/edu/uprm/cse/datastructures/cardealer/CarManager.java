package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;              
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		SortedList<Car> carList = CarList.getCarBook().getCarList();
		carList.add(car);
		return Response.status(201).build();
	}  


	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		SortedList<Car> carList = CarList.getCarBook().getCarList();
		if(carList.isEmpty())
			return null;
		Car[] carsArray = new Car[carList.size()];
		for(int i=0; i<carsArray.length; i++) {
			carsArray[i] = carList.get(i);
		}
		return carsArray;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
	SortedList<Car> carList = CarList.getCarBook().getCarList();
		for(Car car: carList) {
			if(car.getCarId() == id)
				return car;
		}
		throw new NotFoundException(new JsonError("404", " (Not Found)"));
	}   

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car carChanges){
		SortedList<Car> carList = CarList.getCarBook().getCarList();
		Car carToUpdate = null;
		for(Car car: carList) {
			if(car.getCarId() == id)
				carToUpdate = car;
		}
		if(carToUpdate == null)
			return Response.status(Response.Status.NOT_FOUND).build();    
		carList.remove(carToUpdate);
		carList.add(carChanges);
		return Response.status(Response.Status.OK).build();
	}


	@DELETE
	@Path("/{id}/remove")
	public void deleteCar(@PathParam("id") long id){
		SortedList<Car> carList = CarList.getCarBook().getCarList();
		boolean removed = false;
		for(Car car: carList) {
			if(car.getCarId() == id) {
				carList.remove(car);
				removed = true;
			}

		}			
		if(!removed)
			throw new NotFoundException(new JsonError("404", " (Not Found)"));
	}
	
}
