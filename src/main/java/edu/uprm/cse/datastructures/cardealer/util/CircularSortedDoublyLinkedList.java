package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;
import java.util.Iterator;



public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	private Comparator<E> comparator;
	private Node<E> header;
	private int size;
	
	private static class Node<E> {
		private E element;
		private Node<E> prev;
		private Node<E> next;
		
		public Node(E e) {
			this(e, null, null);
		}
		
		public Node(E element, Node<E> prev, Node<E> next) {
			this.element = element;
			this.prev = prev;
			this.next = next;
		}
		
		public Node<E> getPrev(){
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		public Node<E> getNext(){
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public E getElement() {
			return element;
		}
		public void clean() {
			prev = next = null;
			element = null;
		}
	}
	
	public CircularSortedDoublyLinkedList() {
		this.header = new Node<E>(null, header, header);
		this.size = 0 ;
	}

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.size = 0;
		this.header = new Node<E>(null,header,header);
		this.comparator = comparator;
	}

	@Override
	public Iterator<E> iterator() {
		return new SortedListIterator();
	}

	private class SortedListIterator implements Iterator<E> {
		Node<E> curr = header;

		public boolean hasNext() { 
			return curr.getNext() != header; 
		}

		public E next() {
			if (!hasNext())
				return null;
			curr = curr.getNext();
			return curr.getElement();
		}


		public void remove() {
			throw new UnsupportedOperationException("Removal logic not implemented."); }
	}

	@Override
	public boolean add(E obj) {
		Node<E> newNode = new  Node<E>(obj);
		if(this.isEmpty()) {
			header.setNext(newNode);
			header.setPrev(newNode);	
			newNode.setNext(header);
			newNode.setPrev(header);
		} else {
			Node<E> curr = header.getPrev();
			E elmtNewNode=  newNode.getElement();
			while(curr != header) {
				E elementCurrentNode =  curr.getElement();
				if (!(comparator.compare(elementCurrentNode, elmtNewNode) > 0)){
					break;
				}
				curr = curr.getPrev();;
			}
			curr.getNext().setPrev(newNode);
			newNode.setNext(curr.getNext());
			newNode.setPrev(curr);
			curr.setNext(newNode);
		}
		size++;
		return true;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		if(obj == null) 
			return false;

		Node<E> curr = header.getNext();

		while(curr != header) {
			if(comparator.compare(obj, curr.getElement()) == 0) {
				curr.getPrev().setNext(curr.getNext());
				curr.getNext().setPrev(curr.getPrev());
				curr.clean();
				size--;
				return true;
			}
			curr = curr.getNext();	
		}
		return false;
	}


	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= size)
	throw new IndexOutOfBoundsException("remove(index): index out of bounds");
		Node<E> curr = header.getNext();
		for (int i = 0; i < index; i++) {
			curr = curr.getNext();
		}
		curr.getPrev().setNext(curr.getNext());
		curr.getNext().setPrev(curr.getPrev());
		curr.clean();
		size--;
		return true;
	}
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.contains(obj)) {
			this.remove(obj);
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		if (isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		if (isEmpty()) {
			return null;
		}
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		if(index < 0 || index >= size) 
	throw new IndexOutOfBoundsException("Invalid index= " + index);
		Node<E> curr = header;
		for (int i = 0; i <=index; i++) {
				curr = curr.getNext();
		} 
		return curr.getElement();
	}

	@Override
	public void clear() {
		if(isEmpty())
			return;
		Node<E> curr = header.getNext();
		while(curr.getNext() != header) {
			Node<E> nTR = curr;
			curr = curr.getNext();
			nTR.clean();
		}
		curr.clean();
		this.size = 0;
	}

	@Override
	public boolean contains(E e) {
		if(this.isEmpty())
			return false;
		Node<E> curr = header;
		while(curr.getNext() != header) {
			curr = curr.getNext();
			if(curr.getElement().equals(e))
				return true;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public int firstIndex(E e) {
		if(!this.contains(e))
			return -1;
		Node<E> curr = header.getNext();
		int index = 0;
		E elmt=  e;
		E currEmnt = curr.getElement();
		while(!(comparator.compare(currEmnt, elmt) == 0)) {
			curr = curr.getNext();
			currEmnt =  curr.getElement();
			index++;
		}
		return index;
	}

	@Override
	public int lastIndex(E e) {
		if(!this.contains(e))
			return -1;
		Node<E> curr = header.getPrev();
		int index = size-1;
		E elmt=  e;
		E currElmt =  curr.getElement();
		while(!(comparator.compare(currElmt, elmt) == 0)) {
			curr = curr.getPrev();
			currElmt =  curr.getElement();
			index--;
		}
		return index;
	}

	

}
