package edu.uprm.cse.datastructures.cardealer.model;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


public class CarList {
	
	private static CarList carBook = new CarList();
	private  SortedList<Car> cList = null;
	
	private CarList() {
		cList = new CircularSortedDoublyLinkedList<Car>();
	}
	
	public static CarList getCarBook() {
		return carBook;
	}
	
	public  SortedList<Car> getCarList(){
		return this.cList;
	}
	
	public static void resetCars() {
		getCarBook().cList.clear();
		
	}

}